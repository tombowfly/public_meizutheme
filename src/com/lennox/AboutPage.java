/*
 * Copyright (C) 2014 Lennox Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lennox.theme;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.lennox.preferences.PreferenceUtils;

import com.pollfish.interfaces.PollfishSurveyReceivedListener;
import com.pollfish.interfaces.PollfishSurveyNotAvailableListener;
import com.pollfish.interfaces.PollfishSurveyCompletedListener;
import com.pollfish.interfaces.PollfishOpenedListener;
import com.pollfish.interfaces.PollfishClosedListener;
import com.lennox.ads.AdsHelper;
import com.lennox.ads.PollFishHelper;

import com.lennox.theme.meizu.R;

public class AboutPage extends PreferenceActivity implements
        PollfishSurveyReceivedListener, PollfishOpenedListener,
        PollfishSurveyNotAvailableListener, PollfishSurveyCompletedListener, PollfishClosedListener {

    private static final String TAG = "AboutPage";

    protected final Handler mHandler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.about_page);

        AdsHelper.adBanner(this, true);

        ActionBar bar = getActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
        }

        PreferenceUtils.initialiseAboutCategory(this);
     }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            // Action bar home button selected
            onBackPressed();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        AdsHelper.onResume(this, mHandler);
        AdsHelper.adBanner(this, false);
    }

    @Override
    public void onPause() {
        super.onPause();
        AdsHelper.onPause(mHandler);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AdsHelper.onDestroy();
        PreferenceUtils.onDestroy(this);
    }

    // PollFish Helpers

    @Override
    public void onPollfishSurveyReceived(boolean shortSurveys, int surveyPrice) {
        PollFishHelper.onPollfishSurveyReceived(this, mHandler, shortSurveys, surveyPrice);
    }

    @Override
    public void onPollfishSurveyNotAvailable() {
        PollFishHelper.onPollfishSurveyNotAvailable(this, mHandler);
    }

    @Override
    public void onPollfishSurveyCompleted(boolean shortSurveys , int surveyPrice) {
        PollFishHelper.onPollfishSurveyCompleted(this, mHandler, shortSurveys, surveyPrice);
    }

    @Override
    public void onPollfishOpened () {
        PollFishHelper.onPollfishOpened(this, mHandler);
    }

    @Override
    public void onPollfishClosed () {
        PollFishHelper.onPollfishClosed(this, mHandler);
    }

}
