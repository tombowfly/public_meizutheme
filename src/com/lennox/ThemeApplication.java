package com.lennox.theme;

import android.content.Context;
import android.content.Intent;

import lennox.acra.AcraApplication;

import com.lennox.ads.AdsHelper;

public final class ThemeApplication extends AcraApplication {

    private static final String TAG = "ThemeApplication";

    private static final String SETTINGS_KEY = "theme_preferences";

    //Static resources
    private static ThemeApplication sApp;

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        //Save the static application reference
        sApp = this;
        reloadPreferences();
        AdsHelper.init(sApp, SETTINGS_KEY);
    }

    public static void reloadPreferences() {
        // Load all preferences
        synchronized (sApp) {
            ThemeSettingsProvider.load(sApp, SETTINGS_KEY);
        }
    }

    public static void reloadSafePreferences() {
        // Load all preferences
        synchronized (sApp) {
            ThemeSettingsProvider.loadSafe(sApp, SETTINGS_KEY);
        }
    }

}
