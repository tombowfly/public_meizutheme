/*
 * Copyright (C) 2014 Lennox Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lennox.theme;

import android.app.ActionBar;
import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Gallery;

import java.util.ArrayList;

import com.lennox.theme.meizu.R;

import com.pollfish.interfaces.PollfishSurveyReceivedListener;
import com.pollfish.interfaces.PollfishSurveyNotAvailableListener;
import com.pollfish.interfaces.PollfishSurveyCompletedListener;
import com.pollfish.interfaces.PollfishOpenedListener;
import com.pollfish.interfaces.PollfishClosedListener;
import com.lennox.ads.AdsHelper;
import com.lennox.ads.PollFishHelper;

public class AnalogAppWidgetConfigure extends Activity implements AdapterView.OnItemSelectedListener,
        OnClickListener,
        PollfishSurveyReceivedListener, PollfishOpenedListener,
        PollfishSurveyNotAvailableListener, PollfishSurveyCompletedListener, PollfishClosedListener {

    private static final String LOG_TAG = "AnalogAppWidgetConfigure";

    private static final String PREFS_NAME
            = "com.lennox.alarmclock.AnalogAppWidget";

    private static final String PREF_PREFIX_KEY = "clock_style_";

    int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

    int selectedPos = 0;

    Gallery galleryClockStyle;
    ImageView imagePreview;

    private int mClocks;

    protected final Handler mHandler = new Handler();

    public AnalogAppWidgetConfigure() {
        super();
    }

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        // Set the result to CANCELED.  This will cause the widget host to cancel
        // out of the widget placement if they press the back button.
        setResult(RESULT_CANCELED);

        findClockDials();

        setContentView(R.layout.clock_config);

        AdsHelper.adBanner(this, false);

        galleryClockStyle = (Gallery) findViewById(R.id.gallerySelectBackground);
        galleryClockStyle.setAdapter(new ImageAdapter(this));
        galleryClockStyle.setOnItemSelectedListener(this);
        galleryClockStyle.setCallbackDuringFling(false);

        // Find the widget id from the intent.
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        imagePreview = (ImageView) findViewById(R.id.imageViewPreview);

        // If they gave us an intent without the widget id, just bail.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }

        selectedPos = loadClockPref(this, mAppWidgetId);

        galleryClockStyle.setSelection(selectedPos);

        imagePreview.setImageDrawable(getDrawable(this, "clock_" + selectedPos + "_preview"));

        final ActionBar actionBar = getActionBar();
        actionBar.setCustomView(R.layout.actionbar_set_clock);
        actionBar.getCustomView().setOnClickListener(this);
        actionBar.getCustomView().findViewById(R.id.infoActionBar).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(AnalogAppWidgetConfigure.this, AboutPage.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        startActivity(intent);
                    }
                });
    }

    private void findClockDials() {
        final Resources resources = getResources();
        final String packageName = getApplication().getPackageName();
        addClockDials(resources, packageName);
    }

    private void addClockDials(Resources resources, String packageName) {
        for ( int i = 0; i < Integer.MAX_VALUE; i++ ) {
            int clockRes = resources.getIdentifier("clock_" + i, "layout", packageName);
            if (clockRes == 0) {
                mClocks = i;
                break;
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        AdsHelper.onPause(mHandler);
        if (l.b.DEBUG) Log.d(LOG_TAG, "onPause");

    }

    @Override
    public void onStop() {
        super.onStop();
        if (l.b.DEBUG) Log.d(LOG_TAG, "onStop");

    }

    @Override
    public void onResume() {
        super.onResume();
        AdsHelper.onResume(this, mHandler);
        AdsHelper.adBanner(this, false);
        if (l.b.DEBUG) Log.d(LOG_TAG, "onResume");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AdsHelper.onDestroy();
        if (l.b.DEBUG) Log.d(LOG_TAG, "onDestroy");

    }

    public void onItemSelected(AdapterView parent, View v, int position, long id) {
        selectedPos = position;
        imagePreview.setImageDrawable(getDrawable(this, "clock_" + selectedPos + "_preview"));
    }

    public static Drawable getDrawable(Context context, String resourceName) {
        String packageName = context.getPackageName();
        Resources appResources = context.getResources();
        if (appResources != null) {
            int resourceID = appResources.getIdentifier(resourceName, "drawable", packageName);
            if (resourceID != 0) {
               return appResources.getDrawable(resourceID);
            }
        }
        return null;
    }

    public void onNothingSelected(AdapterView parent) {
    }

    private class ImageAdapter extends BaseAdapter {
        private LayoutInflater mLayoutInflater;
        private Context mContext;

        ImageAdapter(AnalogAppWidgetConfigure context) {
            mContext = context;
            mLayoutInflater = context.getLayoutInflater();
        }

        public int getCount() {
            return mClocks;
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView image;

            if (convertView == null) {
                image = (ImageView) mLayoutInflater.inflate(R.layout.clock_config_dial_item, parent, false);
            } else {
                image = (ImageView) convertView;
            }

            Drawable imageDrawable = getDrawable(mContext, "clock_" + position + "_preview");
            if (imageDrawable != null) {
                imageDrawable.setDither(true);
                image.setImageDrawable(imageDrawable);
            } else {
                Log.e(LOG_TAG, String.format(
                    "Error decoding image res=%s for wallpaper #%d",
                    "clock_" + position + "_preview", position));
            }
            return image;
        }
    }

    public void onClick(View v) {
        saveClockPref(this, mAppWidgetId, selectedPos);
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);

        AnalogAppWidgetProvider.updateAppWidget(this, appWidgetManager,
            mAppWidgetId, "clock_" + selectedPos);

        Intent resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
        setResult(RESULT_OK, resultValue);
        finish();
    }

    // Write the clock style to the SharedPreferences object for this widget
    static void saveClockPref(Context context, int appWidgetId, int pos) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.putInt(PREF_PREFIX_KEY + appWidgetId, pos);
        prefs.commit();
    }

    // Read the clock style from the SharedPreferences object for this widget.
    // If there is no preference saved, get the default
    static int loadClockPref(Context context, int appWidgetId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        return prefs.getInt(PREF_PREFIX_KEY + appWidgetId, 0);
    }

    // delete the clock style from the SharedPreferences object for this widget
    static void deleteClockPref(Context context, int appWidgetId) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.remove(PREF_PREFIX_KEY + appWidgetId);
        prefs.commit();
    }

    // PollFish Helpers

    @Override
    public void onPollfishSurveyReceived(boolean shortSurveys, int surveyPrice) {
        PollFishHelper.onPollfishSurveyReceived(this, mHandler, shortSurveys, surveyPrice);
    }

    @Override
    public void onPollfishSurveyNotAvailable() {
        PollFishHelper.onPollfishSurveyNotAvailable(this, mHandler);
    }

    @Override
    public void onPollfishSurveyCompleted(boolean shortSurveys , int surveyPrice) {
        PollFishHelper.onPollfishSurveyCompleted(this, mHandler, shortSurveys, surveyPrice);
    }

    @Override
    public void onPollfishOpened () {
        PollFishHelper.onPollfishOpened(this, mHandler);
    }

    @Override
    public void onPollfishClosed () {
        PollFishHelper.onPollfishClosed(this, mHandler);
    }

}
